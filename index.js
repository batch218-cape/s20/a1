let number = prompt("Give me a number");
console.log("The number you provided is " + number);

for(let i = number; i >= 0; i--) {
    if(i <= 50) {
        console.log("The current value is " + i + ". Terminating the loop.");
        break;
    }

    if(i % 10 === 0) {
        console.log("The number is divisible by 10. Skipping the number");
        continue;
    }

    if(i % 5 === 0) {
        console.log(i);
    }
}

let myWord = "supercalifragilisticexpialidocious";
let myNewWord = "";

let vowels = ['a', 'e', 'i', 'o', 'u'];
for (let i = 0; i < myWord.length; i++) {
    if (vowels.includes(myWord[i].toLocaleLowerCase())) {
        continue;
    } else {
        myNewWord = myNewWord + myWord[i];
    }
}

console.log(myWord);
console.log(myNewWord);
